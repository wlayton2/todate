package unixtime

import (
	"fmt"
	"strconv"
	"time"
)

func ToDate(input []string) {
	output := make([]string, 0)
	for i := 0; i < len(input); i++ {
		formatted := format(input[i])
		output = append(output, formatted)
	}

	fmt.Println(output)
}

func format(value string) string {
	secs, err := strconv.ParseInt(value, 10, 64)
	if err != nil {
		panic(err)
	}

	output := time.Unix(secs, 0)

	return output.Format(time.RFC3339)
}
