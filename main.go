package main

import (
	"bufio"
	"os"
	"strings"

	"gitlab.com/wlayton2/todate/unixtime"
)

func main() {
	info, err := os.Stdin.Stat()
	if err != nil {
		panic(err)
	}

	// Check if piped input is present
	if info.Mode()&os.ModeCharDevice != 0 {
		unixtime.ToDate(os.Args[1:])
		return
	}

	// Scan standard in
	input := make([]string, 0)
	scanner := bufio.NewScanner(os.Stdin)

	for scanner.Scan() {
		text := scanner.Text()
		input = append(input, strings.Split(text, " ")...)
	}

	if err := scanner.Err(); err != nil {
		panic(err)
	}

	unixtime.ToDate(input)
}
